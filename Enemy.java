import java.awt.Color;
import javax.swing.JPanel;

public class Enemy extends Racket {
    public int enemyY;
    
    public Enemy(int enemyY) {
        super(enemyY);
        this.enemyY = enemyY;
    }
    
    public void autoaim(int ballX) {
        if (ballX > Field.width - racketWidth/2) {
            ballX = Field.width - racketWidth/2;
        } else if (ballX < racketWidth/2) {
            ballX = racketWidth/2;
        }
        setLocation((ballX - racketWidth / 2), enemyY);
    }
}
