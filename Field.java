import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;

import javax.swing.JPanel;
import javax.swing.Timer;

public class Field extends JPanel {

    public static int width, height;
    public Racket racket;
    public Enemy enemy;
    public static Ball ball;
    public Timer timer;
    public MouseAdapter mouse;


    public Field(int width, int height) {
        this.width = width;
        this.height = height;

        setBackground(Color.orange);
        setPreferredSize(new Dimension(width, height));
        setLayout(null);

        // add racket
        racket = new Racket(height-35);
        add(racket);
        
        // add enemy
        enemy = new Enemy(10);
        add(enemy);
        
        // add ball
        ball = new Ball();
        add(ball);

        timer = new javax.swing.Timer(5, new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                ball.ballPos();
                ball.collissionWall();
                enemy.autoaim(ball.getX());
                collissionRacket();
            }
        });
        timer.setRepeats(true);
    }

    public void collissionRacket() {
        if (ball.getY() > (racket.getY() - ball.ballSize) && ball.getX() - (ball.ballSize/2) > racket.getX() && ball.getX() + (ball.ballSize/2) < (racket.getX() + racket.racketWidth)) {
            ball.velY = - ball.velY;
        }
    }

}
