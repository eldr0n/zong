/**
 * main class for pong game
 * 
 * @author Rico Good 
 * @version 0.2
 * 
 * TODO:
 *  + moar comments 
 *  + fix player racket/ball collision
 *  + add difficulty levels
 *  + fight capitalism
 *
 */
import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

public class Zong extends JFrame {
    private JButton btnStart;
    private Field field;
    private MouseAdapter mouse;

    public Zong() {
        initWindow();
    }

    private void initWindow() {
        setTitle("Pong named Zong");
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        contentPane.add(northLayout(), BorderLayout.NORTH);

        field = new Field(720, 960);
        add(field, BorderLayout.CENTER);

        pack();
        setVisible(true);

    }

    private JPanel northLayout() {
        JPanel panel = new JPanel();
        btnStart = new JButton("Start");
        panel.setLayout(new FlowLayout(FlowLayout.LEADING));
        panel.add(btnStart);

        btnStart.addActionListener(e -> {gameStart();});
        
        return panel;
    }

    public void gameStart() {
        field.timer.start();
        addMouseMotionListener(mouse = new MouseAdapter() {
            public void mouseMoved(MouseEvent e) {
                field.racket.move(e.getX());
            }
        });
    }
}
