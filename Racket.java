import java.awt.Color;
import javax.swing.JPanel;

public class Racket extends JPanel {
  
    
    public int racketWidth;
    public int racketHeight;
    public int racketX;
    public int racketY;

    public Racket(int racketY) {
        racketHeight = 25;
        racketWidth = 160;
        this.racketY = racketY;
        setBackground(Color.white);
        setBounds(0, 0, racketWidth, racketHeight);
        init(racketY);
    }
    
    public void init(int racketY) {
            racketX = (Field.width - racketWidth) / 2;
            setLocation(racketX, racketY);

    }

    public void move(int mouseX) {

        if (mouseX > Field.width - racketWidth) {
            mouseX = Field.width - racketWidth;
        }
        setLocation(mouseX, racketY);
    }
}
