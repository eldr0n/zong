import java.awt.Color;
import javax.swing.JPanel;

public class Ball extends JPanel {
        
    public int ballX, ballY, velX, velY, ballSize; 


    public Ball() {
        ballSize = 30;
        velX = 2;
        velY = 2;
        
        setBackground(Color.blue);
        setBounds(0, 0, ballSize, ballSize);

        init();
    }

    public void init() {
        ballX = (Field.width - ballSize) / 2;
        ballY = (Field.height - ballSize) / 2;
        setLocation(ballX, ballY);
    }

    public void ballPos() {
        ballX = Field.ball.getX() + velX;
        ballY = Field.ball.getY() + velY;
        setLocation(ballX, ballY);
    }
    
    public void collissionWall() {
        if (ballX >= Field.width - (ballSize / 2) || ballX <= (ballSize / 2)) {
            velX = - velX;
        }
        else if (ballY <= (ballSize / 2)+ 10) {
            velY = - velY;
        }
        else if (ballY >= Field.height) {
            init();
        }
    }
}
